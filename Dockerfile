FROM ubuntu:16.04
MAINTAINER Rich Choy <rich@it-consultis.com>

ENV DEBIAN_FRONTEND noninteractive
ENV LC_ALL en_US.UTF-8 
ENV LANG en_US.UTF-8

ENV RUNTIME_USER runtime
ENV RUNTIME_GROUP runtime
ENV RUNTIME_HOME /home/runtime

ENV NODE_PLATFORM linux-x64
ENV NODE_VERSION v12.10.0
ENV NODE_USER runtime
ENV NODE_GROUP runtime

ENV NODE_HOME $RUNTIME_HOME
ENV NODE_PREFIX /opt/node

ENV PROJECT_ROOT /project

COPY ./artifacts /tmp/artifacts

RUN set -xeu \
    && apt-get update \
    && apt-get install -y \
        build-essential \
        curl \
        fakeroot \
        g++ \
        locales \
        make \
        python \
        software-properties-common \
    && locale-gen en_US.UTF-8 \
    && dpkg-reconfigure locales \
    && echo "installing node and npm binaries" \
    && cd /opt \
    && curl --insecure https://nodejs.org/dist/${NODE_VERSION}/node-${NODE_VERSION}-${NODE_PLATFORM}.tar.xz > node.tar.xz \
    && tar -xJf node.tar.xz \
    && rm node.tar.xz \
    && mv node-${NODE_VERSION}-${NODE_PLATFORM} node \
    && ln -s $NODE_PREFIX/bin/node /usr/local/bin/node \
    && ln -s $NODE_PREFIX/lib/node_modules/npm/bin/npm-cli.js /usr/local/bin/npm \
    && echo "setting up run user and group" \
    && chmod 777 /tmp \
    && mkdir -p $RUNTIME_HOME \
    && groupadd $RUNTIME_GROUP \
    && useradd --home-dir $RUNTIME_HOME -g $RUNTIME_GROUP $RUNTIME_USER \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $RUNTIME_HOME \
    && echo "setting up project mount point" \
    && mkdir $PROJECT_ROOT \
    && echo "You need to mount this directory." > $PROJECT_ROOT/README.md \
    && chown -R $RUNTIME_USER:$RUNTIME_GROUP $PROJECT_ROOT \
    && echo "setting up entrypoint" \
    && cp /tmp/artifacts/docker-entrypoint.sh /docker-entrypoint.sh \
    && chmod 755 /docker-entrypoint.sh \
    && echo "unsafe-perm = true" >> /root/.npmrc \
    && echo "cleaning up" \
    && apt-get clean -y \
    && apt-get autoremove -y \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

ENTRYPOINT ["/docker-entrypoint.sh"]
WORKDIR $PROJECT_ROOT
VOLUME [$PROJECT_ROOT]

# retain this or downstream node-gyp operations will fail
ENV USER root

