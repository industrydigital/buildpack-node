# buildpack-node

## Features 

- Based on ubuntu:14.04
- node v9.11.1 binary at /usr/local/bin
- npm binary at /usr/local/bin
- all dependencies required for node-gyp to compile C++ node modules
- Unprivileged "runtime" user that has a home directory at /home/runtime
- Assigns DOCKER_HOST_UID and DOCKER_HOST_GID to the run user inside the docker entrypoint

## Run user

```
runtime:runtime
```

## Environment variables

```
DOCKER_HOST_UID - uid to assign to the run user
DOCKER_HOST_GID - gid to assign to the run user
```

## Volumes

```
/project                 mount your project here
```
