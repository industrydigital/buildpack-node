#!/usr/bin/env bash
set -aeu

if [ "$(whoami)" == "root" ]; then
    if [ -v "DOCKER_HOST_UID" ]; then
        usermod -u $DOCKER_HOST_UID $RUNTIME_USER
    fi
    if [ -v "DOCKER_HOST_GID" ]; then
        groupmod -g $DOCKER_HOST_GID $RUNTIME_GROUP || true
    fi
    chown -R $RUNTIME_USER:$RUNTIME_GROUP $RUNTIME_HOME
fi

exec "$@"

